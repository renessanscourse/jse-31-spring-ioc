package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.UserEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class UserUpdatePasswordCommand extends AbstractCommand {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.UPDATE_PASSWORD;
    }

    @NotNull
    @Override
    public String description() {
        return "Update password to your account";
    }

    @Override
    public void execute() {
        System.out.println("[PASSWORD CHANGE]");
        System.out.print("ENTER YOUR CURRENT PASSWORD: ");
        @Nullable final String currentPassword = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PASSWORD: ");
        @Nullable final String newPassword = TerminalUtil.nextLine();
        userEndpoint.updatePassword(sessionDTO, currentPassword, newPassword);
    }

}