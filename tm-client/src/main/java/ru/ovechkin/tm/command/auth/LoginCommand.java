package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.SessionDTO;
import ru.ovechkin.tm.endpoint.SessionEndpoint;
import ru.ovechkin.tm.util.TerminalUtil;

@Component
public class LoginCommand extends AbstractCommand {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGIN;
    }

    @NotNull
    @Override
    public String description() {
        return "Login in your account";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.print("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.print("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("LOGGING IN...");
        @NotNull final SessionDTO sessionFromServer = sessionEndpoint.openSession(login, password);
        sessionDTO.setId(sessionFromServer.getId());
        sessionDTO.setUserId(sessionFromServer.getUserId());
        sessionDTO.setSignature(sessionFromServer.getSignature());
        sessionDTO.setTimestamp(sessionFromServer.getTimestamp());
        System.out.println("[OK]");
    }

}