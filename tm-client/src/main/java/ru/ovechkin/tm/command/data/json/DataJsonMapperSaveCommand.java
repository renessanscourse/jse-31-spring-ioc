package ru.ovechkin.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.StorageEndpoint;

@Component
public class DataJsonMapperSaveCommand extends AbstractCommand {

    @Autowired
    private StorageEndpoint storageEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_JSON_MAPPER_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to json file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON SAVE]");
        storageEndpoint.dataJsonMapperSave(sessionDTO);
        System.out.println("[OK]");
    }

}