package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.config.ClientConfiguration;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.Collection;

@Component
public final class CommandsAllCommand extends AbstractCommand {

    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_COMMANDS;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_COMMANDS;
    }

    @NotNull
    @Override
    public String description() {
        return "Show available commands";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@NotNull final AbstractCommand command : commands) System.out.println(command.name());
    }

}