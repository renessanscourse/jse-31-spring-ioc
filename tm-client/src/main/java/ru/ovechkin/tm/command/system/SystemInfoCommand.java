package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.util.NumberUtil;

@Component
public final class SystemInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_INFO;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_INFO;
    }

    @NotNull
    @Override
    public String description() {
        return "Display system's info";
    }

    @Override
    public void execute() {
        System.out.println("[INFO]");

        @NotNull final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        @NotNull final long freeMemory = Runtime.getRuntime().freeMemory();
        @NotNull final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);

        @NotNull final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory (bytes): " + maxMemoryFormat);

        @NotNull final long totalMemory = Runtime.getRuntime().totalMemory();
        @NotNull final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM (bytes): " + totalMemoryFormat);

        @NotNull final long usedMemory = totalMemory - freeMemory;
        @NotNull final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM (bytes): " + usedMemoryFormat);
    }

}