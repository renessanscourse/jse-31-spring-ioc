package ru.ovechkin.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.config.ClientConfiguration;

import java.lang.Exception;

public class Client {

    public static void main(String[] args) throws Exception {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}