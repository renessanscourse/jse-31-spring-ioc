package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ProjectRepository implements IProjectRepository {

    @NotNull
    private EntityManager entityManager;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public final void add(@NotNull final Project project) {
        entityManager.persist(project);
    }

    @Nullable
    @Override
    public Project findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId AND id = :id", Project.class)
                .setMaxResults(1);
        query.setParameter("userId", userId);
        query.setParameter("id", id);
        @NotNull final Project project = query.getSingleResult();
        return project;
    }

    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId AND name = :name", Project.class)
                .setMaxResults(1);
        query.setParameter("userId", userId);
        query.setParameter("name", name);
        @NotNull final Project project = query.getSingleResult();
        return project;
    }

    @Nullable
    @Override
    public final List<Project> findUserProjects(@NotNull final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final TypedQuery<Project> query = entityManager.createQuery(
                "FROM Project WHERE user_id = :userId", Project.class)
                .setMaxResults(1);
        query.setParameter("userId", userId);
        @Nullable final List<Project> projects = query.getResultList();
        return projects;
    }

    @Override
    public final void removeAll(@NotNull final String userId) {
        @NotNull final Query query = entityManager.createQuery(
                "DELETE Project WHERE user_id = :userId");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        entityManager.remove(project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> getAllProjects() {
        @Nullable final TypedQuery<Project> query = entityManager.createQuery("FROM Project", Project.class);
        return query.getResultList();
    }

    @NotNull
    @Override
    public List<Project> mergeCollection(@NotNull final List<Project> projectList) {
        for (@NotNull final Project project : projectList) {
            entityManager.persist(project);
        }
        return projectList;
    }

    @Override
    public void removeAllProjects() {
        final Query query = entityManager.createQuery("DELETE FROM Project");
        query.executeUpdate();
    }

}