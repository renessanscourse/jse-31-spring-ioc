package ru.ovechkin.tm.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public final class UserDTO extends AbstractDTO implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    public UserDTO(@NotNull String login, @NotNull String passwordHash, @NotNull Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public UserDTO(@NotNull String login, @NotNull String passwordHash, @NotNull String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{\n" +
                "login='" + login + '\n' +
                "passwordHash='" + passwordHash + '\n' +
                "email='" + email + '\n' +
                "firstName='" + firstName + '\n' +
                "lastName='" + lastName + '\n' +
                "middleName='" + middleName + '\n' +
                "role=" + role + '\n' +
                "locked=" + locked + '\n' +
                "id=" + getId() + '\n' +
                '}';
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NotNull String passwordHash) {
        this.passwordHash = passwordHash;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NotNull String email) {
        this.email = email;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NotNull String firstName) {
        this.firstName = firstName;
    }

    @NotNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NotNull String lastName) {
        this.lastName = lastName;
    }

    @NotNull
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@NotNull String middleName) {
        this.middleName = middleName;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

    @NotNull
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

    @Nullable
    public static UserDTO toDTO(@Nullable User user) {
        if (user == null) return  null;
        return new UserDTO(user);
    }

    @NotNull
    public static List<UserDTO> toDTO(@Nullable final Collection<User> users) {
        if (users == null || users.isEmpty()) return Collections.emptyList();
        @Nullable final List<UserDTO> result = new ArrayList<>();
        for (@Nullable final User user : users) {
            if (user == null) continue;
            result.add(new UserDTO(user));
        }
        return result;
    }

    public UserDTO(@Nullable final User user) {
        if (user == null) return;
        setId(user.getId());
        setLogin(user.getLogin());
        setPasswordHash(user.getPasswordHash());
        setRole(user.getRole());
        setLocked(user.getLocked());
    }

}