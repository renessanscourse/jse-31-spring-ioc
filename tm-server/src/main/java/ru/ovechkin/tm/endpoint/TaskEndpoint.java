package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.ovechkin.tm.api.endpoint.ITaskEndpoint;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.TaskDTO;
import ru.ovechkin.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private TaskService taskService;

    public TaskEndpoint() {
    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "task", partName = "task") TaskDTO taskDTO,
            @Nullable @WebParam(name = "project", partName = "project") ProjectDTO projectDTO
    ) {
        sessionService.validate(sessionDTO);
        taskService.add(sessionDTO, taskDTO, projectDTO);
    }

    @Override
    @WebMethod
    public void createTaskWithName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId
    ) {
        sessionService.validate(sessionDTO);
        taskService.create(sessionDTO, taskName, projectId);
    }

    @Override
    @WebMethod
    public void createTaskWithNameAndDescription(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "taskName", partName = "taskName") final String taskName,
            @Nullable @WebParam(name = "taskDescription", partName = "taskDescription") final String taskDescription,
            @Nullable @WebParam(name = "projectId", partName = "projectId") final String projectId
    ) {
        sessionService.validate(sessionDTO);
        taskService.create(sessionDTO, taskName, taskDescription, projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public List<TaskDTO> findUserTasks(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        sessionService.validate(sessionDTO);
        return taskService.findUserTasks(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) {
        sessionService.validate(sessionDTO);
        taskService.removeAllUserTasks(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public TaskDTO findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(sessionDTO);
        return taskService.findTaskById(sessionDTO.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(sessionDTO);
        return taskService.findTaskByName(sessionDTO.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        sessionService.validate(sessionDTO);
        return taskService.updateTaskById(sessionDTO.getUserId(), id, name, description);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        sessionService.validate(sessionDTO);
        return taskService.removeTaskById(sessionDTO.getUserId(), id);
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO removeTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        sessionService.validate(sessionDTO);
        return taskService.removeTaskByName(sessionDTO.getUserId(), name);
    }

}